/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geracaoSimulacaoSpice;


import java.io.IOException;
import java.net.URLDecoder;
/**
 *
 * @author Gaci
 */
public class GeracaoSimulacaoSpice {
    
    //GeracaoSimulacaoSpice() throws UnsupportedEncodingException
    public static void main(String[] args) throws IOException
    {
        int nSimulacoes;
        String sistemaOperac = System.getProperty("os.name").toString();
        
        String filePathLeituraEntrada;
        
        /*SETAR AQUI O Número de arquivos .bats que serao criados*/
        nSimulacoes = 1;
        
        //String filePathEscrita   = "C:\\Users\\Vitor\\Documents\\vitor_CTCI\\geracaosimulacaospice\\src\\simulacao3\\dimensionamento_xor\\"
        //        + "simulation\\";
        
        String path = System.getProperty("java.class.path");
        String filePathEscrita = URLDecoder.decode(path, "UTF-8");
        
        filePathEscrita = filePathEscrita.replaceAll("geracao.simulacao.spice.jar", "");
        filePathEscrita = filePathEscrita.split(";")[0];
                
        //teste
        /**************VARIADOS*************/
//         filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/CMOS/xor/";
         
        /**************NAND BSTTL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/BSTTL/nand/";
        /**************NOR BSTTL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/BSTTL/nor/";

        /**************NAND BSTTL MVT*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/BSTTL/nand_MVT/";
        /**************NOR BSTTL MVT*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/BSTTL/nor_MVT/";

        /**************NAND BSTTL MVT invSupNormal*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/BSTTL/nand_MVT_invSupNormal/";
        /**************NOR BSTTL MVT invSupNormal*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/BSTTL/nor_MVT_invSupNormal/";

        /**************NAND BSTTL SV_2inv*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/BSTTL/nand_SV_2inv/";
        /**************NOR BSTTL SV_2inv*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/BSTTL/nor_SV_2inv/";

        /**************NAND BSTTL SV_4inv*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/BSTTL/nand_SV_4inv/";
        /**************NOR BSTTL SV_4inv*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/BSTTL/nor_SV_4inv/";        
        
        /**************NAND STTL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/nand/";
        /**************NOR STTL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/nor/";
        /**************XOR STTL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/xor/";

        /**************NAND STTL MVT*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/nand_MVT/";
        /**************NOR STTL MVT*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/nor_MVT/";
        /**************XOR STTL MVT*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/xor_MVT/";
        
        /**************NAND STTL MVT invSupNormal*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/nand_MVT_invSupNormal/";
        /**************NOR STTL MVT invSupNormal*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/nor_MVT_invSupNormal/";
        /**************XOR STTL MVT invSupNormal*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/xor_MVT_invSupNormal/";

        /**************NAND STTL SV_2inv*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/nand_SV_2inv/";
        /**************NOR STTL SV_2inv*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/nor_SV_2inv/";
        /**************XOR STTL SV_2inv*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/xor_SV_2inv/";

        /**************NAND STTL SV_4inv*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/nand_SV_4inv/";
        /**************NOR STTL SV_4inv*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/nor_SV_4inv/";
        /**************XOR STTL SV_4inv*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/xor_SV_4inv/";

        /**************NAND DPPL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/DPPL/nand/";;
        /**************NOR DPPL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/DPPL/nor/";
        /**************XOR DPPL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/DPPL/xor/";
        
        /**************NAND PCSL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/PCSL/nand/";
        /**************NOR PCSL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/PCSL/nor/";;;
        /**************XOR PCSL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/PCSL/xor/";
        
        /**************NAND WDDL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/WDDL/nand/";
        /**************NOR WDDL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/WDDL/nor/";
        /**************XOR WDDL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/WDDL/xor/";;

        /**************NAND SABL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/SABL/nand/";
        /**************NOR SABL*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/SABL/nor/";
        
        /**************ISTTL_c/ Or*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/ISTTL_nandPorOr/nand/";
        
        /**************NAND STTL TESTE_SUPPLY_LATCHES*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/justificativa_latches/nand/";
        /**************NAND STTL_MVT TESTE_SUPPLY_LATCHES*************/
//        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/justificativa_latches/nand_MVT/";
        
        /**************NAND STTL ST*************/
        filePathEscrita = "/home/vgdlima/Documents/Vitor_Pesquisa/Portas/TSMC40nm/STTL/nand_ST/";

        //teste
        if(sistemaOperac.toLowerCase().equals("linux"))
            filePathLeituraEntrada   = filePathEscrita + "/entrada.sp";
        else
            filePathLeituraEntrada   = filePathEscrita + "/entrada.sp";
        
        String prefixFile = "arq";
        ManipularArquivosSimulacao gerarSimulacao = new ManipularArquivosSimulacao(filePathLeituraEntrada, filePathEscrita, prefixFile);
        
        if(sistemaOperac.toLowerCase().equals("linux"))
            gerarSimulacao.geracaoBatLinux(filePathEscrita, prefixFile, gerarSimulacao.getTotArquivos(), nSimulacoes);
        else
            gerarSimulacao.geracaoBatWindows(filePathEscrita, prefixFile, gerarSimulacao.getTotArquivos(), nSimulacoes);
    }
    
}
