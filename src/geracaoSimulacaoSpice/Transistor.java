/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package geracaoSimulacaoSpice;

/**
 *
 * @author Vitor
 */
public class Transistor 
{
    private int iteracoes;    
    private float passo;
    
    private String nome; 
    private Double valorInicial;
    
    public Transistor(String nome, String valorInicial, int iteracoes, float passo)
    {
        this.nome = nome;
        this.passo = passo;
        this.iteracoes = iteracoes;
        this.valorInicial = Double.parseDouble(valorInicial);        
    }

    public Transistor(Transistor transistor)
    {
        this.nome = transistor.nome;
        this.passo = transistor.passo;
        this.iteracoes = transistor.iteracoes;
        this.valorInicial = transistor.valorInicial;        
    }
    
    //Método responsável por calcular o valor inicial a partir de uma expressão do cabeçalho.. Utili
    //Utilizar x como multiplicação
    private Double clcVlrIniFromString(String valorInicial)
    {
        String[] valorInicialVetor ;
        Double total = 1d;
        String totalString = "";
        Double temp;
        
        valorInicial = valorInicial.replaceAll(" ", "");
        valorInicialVetor = valorInicial.split("x");
        
        for(int i=0; i<valorInicialVetor.length; i++)        
        {
            total *= Double.parseDouble(valorInicialVetor[i]);
        }
        
        //Verificar e consertar caso tenha erro de parser
        temp = total - total.longValue();
        
        totalString = total.toString();
        
        if(temp.toString().length()>10)
        {
            totalString = totalString.substring(0, totalString.length()-2);
        }
                
        return Double.parseDouble(totalString);
    }
    
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getIteracoes() {
        return iteracoes;
    }

    public void setIteracoes(int iteracoes) {
        this.iteracoes = iteracoes;
    }

    public float getPasso() {
        return passo;
    }

    public void setPasso(float passo) {
        this.passo = passo;
    }

    public void setValorInicial(Double valorInicial) {
        this.valorInicial = valorInicial;
    }

    public Double getValorInicial() {
        return valorInicial;
    }
    
}
