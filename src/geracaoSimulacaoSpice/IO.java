/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geracaoSimulacaoSpice;

import java.io.*;
import java.nio.channels.FileChannel;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author Gaci
 */
public class IO 
{    
    public void escreverArquivo(String filePath, String toWrite)
    {
        File file = new File(filePath);
        
        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream (file), "utf-8"));)
        {          
            Writer writer = bw;
            writer.write(toWrite);
            
            bw.close();
            writer.close();         
        }
        catch(Exception e)
        {
            System.err.println(e);
        }
    }   
    public void copiarArquivo(String sourcePath, String destinationPath)
    {
        File source = new File(sourcePath);
        File destination = new File(destinationPath);
        
        if (destination.exists())
            destination.delete();
        FileChannel sourceChannel = null;
        FileChannel destinationChannel = null;
        
        try {
            sourceChannel = new FileInputStream(source).getChannel();
            destinationChannel = new FileOutputStream(destination).getChannel();
            sourceChannel.transferTo(0, sourceChannel.size(),
                    destinationChannel);
        } 
        catch(Exception e)
        {
            System.err.println(e);
        }
        finally {
            try
            {
                if (sourceChannel != null && sourceChannel.isOpen())
                    sourceChannel.close();
                if (destinationChannel != null && destinationChannel.isOpen())
                    destinationChannel.close();
            }
            catch(Exception e)
            {
                System.err.println(e);
            }
       }        
    }
    public void esvaziarPasta (File f) {
        if (f.isDirectory()) {
            File[] files = f.listFiles();
            for (int i = 0; i < files.length; ++i) {
                esvaziarPasta (files[i]);
            }
        }
        f.delete();
    }
    public void criarPasta(String filePath)
    {
        File file = new File(filePath);
        
        try{
            if(!file.exists())
            {
                file.mkdir();
            }
        }
        catch(Exception e)
        {
            System.err.println(e);
        }
    }
    public String lerArquivo(String filePath)
    {
        String output = null;
        
        try(BufferedReader br = new BufferedReader(new FileReader(filePath))) 
        {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
            }
            
            br.close();
            
            output = sb.toString();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(IO.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(IO.class.getName()).log(Level.SEVERE, null, ex);
        }       
        
        return output;
    }
}