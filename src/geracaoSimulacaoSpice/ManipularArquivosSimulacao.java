/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package geracaoSimulacaoSpice;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.rmi.CORBA.Util;


/**
 *
 * @author Gaci
 */
public class ManipularArquivosSimulacao {

    private IO io;   
    private List<Transistor> listaTransistor;
    private String configurar;
    private String caminhoLeitura;
    private String pastaEscrita;     
    private String arquivoEntradaCompleto;
    private String prefixName;
    private int totArquivos;
    private int nArcosTotal; 
    private int nArcosPCenario;
    private int nVariaveis;    
    private float  passo;

    private HashMap<String, String> transistoresSizing;
    
    public ManipularArquivosSimulacao(String caminhoLeituraEntrada, String pastaEscrita, String prefixName) 
    {
        io = new IO();
        List<String> arquivosTextoSaidas = new ArrayList<>();
        listaTransistor = new ArrayList<>();
        String sistemaOperacional = System.getProperty("os.name");
        
        if(sistemaOperacional.toLowerCase().equals("linux"))
        {
            io.esvaziarPasta(new File(pastaEscrita+"/Spices"));        
        
            io.criarPasta(pastaEscrita+"/Spices");
        }else{
            io.esvaziarPasta(new File(pastaEscrita+"\\Spices"));        
        
            io.criarPasta(pastaEscrita+"\\Spices");
            
        }
        
        this.prefixName = prefixName;
        this.caminhoLeitura = caminhoLeituraEntrada;
        this.pastaEscrita = pastaEscrita;         
        this.arquivoEntradaCompleto = io.lerArquivo(this.caminhoLeitura);       
        this.transistoresSizing = new HashMap<>();
        
        configurarSimulacao();
        arquivosTextoSaidas.addAll(gerarArquivos2Simulacao());
        
        this.totArquivos = arquivosTextoSaidas.size();
        gerarArquivosSaidas(arquivosTextoSaidas, 0, prefixName);        
        
        this.nVariaveis = transistoresSizing.size();
        
        gerarArquivoConfiguracao();
        
        if(!sistemaOperacional.toLowerCase().equals("linux"))
            io.copiarArquivo(pastaEscrita+"\\tspcmd64.exe.lnk", pastaEscrita+"\\Spices\\tspcmd64.exe.lnk");
    }    
    private void gerarArquivoConfiguracao()
    {
        String escArquivo;
        String sistemaOperacional = System.getProperty("os.name");
        
        escArquivo = "nArcos="+this.nArcosPCenario;
        escArquivo += "\nnCenariosSim="+this.nArcosTotal/this.nArcosPCenario;
        
        escArquivo += "\nnArquivos="+this.totArquivos;
        escArquivo += "\nprefixo="+this.prefixName;
        
        if(sistemaOperacional.toLowerCase().equals("linux"))
            io.escreverArquivo(this.pastaEscrita+"/Spices/"+"configuracao.txt", escArquivo);
        else
            io.escreverArquivo(this.pastaEscrita+"\\Spices\\"+"configuracao.txt", escArquivo);
    }
    private void gerarArquivosSaidas(List<String> arquivosTexto, int index, String prefixName)
    {
        String sistemaOperacional = System.getProperty("os.name");
        
        for(int i=0; i<arquivosTexto.size(); i++)
        {
            if(sistemaOperacional.toLowerCase().equals("linux"))
                io.escreverArquivo(this.pastaEscrita+"/Spices/"+prefixName+"_"+(i+index)+".sp", arquivosTexto.get(i));
            else
                io.escreverArquivo(this.pastaEscrita+"\\Spices\\"+prefixName+"_"+(i+index)+".sp", arquivosTexto.get(i));
        }
    }    
    private void configurarSimulacao()
    {
        
        String transistores;
        
        //Pegando os dados de configuração da simulação
        this.configurar = this.arquivoEntradaCompleto.substring(
                                                                this.arquivoEntradaCompleto.indexOf("/*Configurar Simulador"), 
                                                                this.arquivoEntradaCompleto.indexOf("FIMCONFIGURAR*/")
                                                                );
        
        this.configurar = this.configurar.substring(this.configurar.indexOf("#")+2);
        this.configurar = this.configurar.substring(0, this.configurar.indexOf("#")-1);
        
        //Separando dimensionamento dos transistores do resto do spice
        transistores = arquivoEntradaCompleto.split(".INICIOEND")[1];
        transistores = transistores.split(".END")[0];
        
        
        configurarValoresTransistores(transistores);
        configurarValoresConfiguracao();
        
    }    
    //Rotina para receber valores de configuração
    /*Exemplo configuração:
     * 
     * /*Configurar Simulador#
        nArcos=4;
        passo=0.1;

        nome = valor inicial : iterações : step
        Transistores
        nMosSTTL2=0.1u*2.025*valor:5:0.3
        nMosSTTL3=0.1u*1.5*valor:5:0.3
        nMosSTTL1=0.1u*2.025*valor:5:0.3
        nMosSTTL4=0.1u*1.5*valor:5:0.3
        nMosSTTL5=0.1u*2.025*valor:5:0.3
        TransistoresFim
        #
        FIMCONFIGURAR*/
    private void configurarValoresConfiguracao(){
        String[] vetorValores;
        
        vetorValores = this.configurar.split(";");
        
        for(int i=0; i < vetorValores.length; i++)
        {
            String[] vetorTemp;
            
            vetorTemp = vetorValores[i].split("=");
            
            if(vetorTemp[0].contains("nArcosTotal"))
            {
                this.nArcosTotal = Integer.parseInt(vetorTemp[1]);
            }
            
            else if(vetorValores[i].contains("nArcosPCenario"))
            {
                this.nArcosPCenario = Integer.parseInt(vetorTemp[1]);
            }
            
            else if(vetorValores[i].contains("Transistores"))
            {
                configurarTransistores(vetorValores[i]);
            }
            
            else if(vetorTemp[0].contains("passo"))
            {
                this.passo = Float.parseFloat(vetorTemp[1]);
            }            
        }
        
        
    }    
    //Utilizado pela rotina configurarValoresConfiguracao() 
    private void configurarTransistores(String entradaTransistores){
        String[] vetorTemp;
                
        entradaTransistores = entradaTransistores.split("Transistores")[1];
        entradaTransistores = entradaTransistores.split("TransistoresFim")[0];
        
        vetorTemp = entradaTransistores.split("\n");
        
        for(int i=0; i<vetorTemp.length; i++)
        {
            if(!vetorTemp[i].contains("=")){continue;}
            String[] valores = vetorTemp[i].split("=");
            
            String nomeTemp = valores[0];
            
            valores = valores[1].split(":");
            
            String valorInicial = valores[0];
            int iteracoes = Integer.parseInt(valores[1]);
            float passo = Float.parseFloat(valores[2]);
            
            this.listaTransistor.add(new Transistor(nomeTemp, valorInicial, iteracoes, passo));
        }
    }    
    //Rotina para receber valores de configuração
    private void configurarValoresTransistores(String transistores){       
        String[] vetorValores;
        vetorValores = transistores.split("\n");
        List<String> listaValores = new ArrayList<>();
        
        for(int i=0; i<vetorValores.length; i++)
        {
            listaValores.add(vetorValores[i]);
        }
        
        //Retirar posições que não contenham .param
        for(int i=0; i<listaValores.size(); i++)
        {
            if(!listaValores.get(i).contains(".param "))
            {
                listaValores.remove(i);
            }
        }
                
        for(int i=0; i < listaValores.size(); i++)
        {          
            String vetorTemp  = listaValores.get(i).split(".param ")[1];
            String[] param = vetorTemp.split("="); 
            
            this.transistoresSizing.put(param[0], param[1]);            
        }
    }     
    //Método Responsável por gerar as listas de Strings referentes aos arquivos de saída
    public List<String> gerarArquivos2Simulacao(){
        
        int indexSaveFile = 0;
        
        List<Integer> listaIteracoes = new ArrayList<>();
        List<String> arquivosSimulacao = new ArrayList<>();
        int totalIteracoes;
        
        
        totalIteracoes = numIteracoesTransistores();
        
        //Responsável por achar o ponto onde modificará
        String subArcosAntes  = arquivoEntradaCompleto.split(".param caso = '0'")[0];
        String subArcosDepois = arquivoEntradaCompleto.split(".param caso = '0'")[1];      
                
        subArcosDepois = subArcosDepois.split(".INICIOEND")[0];
        
        //Inicializar todos os registradores com step 0
        for(int i=0; i<this.listaTransistor.size(); i++)
        {
            listaIteracoes.add(0);
        }
        
        for(int i=0; i<totalIteracoes; i++)
        {   
            String arquivoSimulacao;
            String transistoresSizing = "";
            
            transistoresSizing = calcTxtDimens(listaIteracoes);
            
            calcularSteps(listaIteracoes);            
                    
            for(int j=0; j<this.nArcosTotal; j++)
            {
                arquivoSimulacao = subArcosAntes + ".param caso = '"+j+"'" + subArcosDepois + 
                                        "*.INICIOEND" + transistoresSizing + "\n.END";
                
                arquivosSimulacao.add(arquivoSimulacao);
            }
        }
        return arquivosSimulacao;
    }    
    //Responsável por gerar texto contendo os dimensionamentos dos transistores
    private String calcTxtDimens(List<Integer> listaIteracoes) {
        String outputString = "";
        
        for(int i=0; i<listaIteracoes.size() ;i++)
        {
            
            Transistor transistorTemp = new Transistor(this.listaTransistor.get(i));
            Double resultado=0d;
            DecimalFormat form;
            String valorTemp;

            form = new DecimalFormat("0.000");
            valorTemp = form.format(transistorTemp.getValorInicial()+(transistorTemp.getPasso() * listaIteracoes.get(i)));
            valorTemp = valorTemp.replaceAll(",", ".");
            resultado = Double.parseDouble(valorTemp);
            //double teste = ((int)(( transistorTemp.getValorInicial()+(transistorTemp.getPasso() * listaIteracoes.get(i)) )*10E5))/10E5;
                                    
            outputString += 
                    "\n" + ".param " + transistorTemp.getNome() + "='" + resultado +"'";
        
        }
                
        return outputString;
    }    
    //Responsável por iterar o step
    private void calcularSteps(List<Integer> listaIteracoes){
        //Controle para descobrir onde subir o step
        for(int k=this.listaTransistor.size()-1; k>=0; k--)
        {
            
            if(listaIteracoes.get(k) == this.listaTransistor.get(k).getIteracoes()-1)
            {
                listaIteracoes.set(k ,0);

                if(k == 0)
                {
                    break;
                }
                else
                {
                    continue;
                }
            }
            else
            {
                listaIteracoes.set(k, listaIteracoes.get(k)+1);
                break;
            }
        }
    
    }   
    //Quantifica a quantidade de iterações necessárias para todos os dimensionamentos
    private int numIteracoesTransistores(){
        int total=1;
        
        for(int i=0; i<this.listaTransistor.size(); i++)
        {
            total = total*this.listaTransistor.get(i).getIteracoes();
        }
        
        return total;
    }    
    public IO getIo() {
        return io;
    }
    public void setIo(IO io) {
        this.io = io;
    }
    public String getCaminhoLeitura() {
        return caminhoLeitura;
    }
    public void setCaminhoLeitura(String caminhoLeitura) {
        this.caminhoLeitura = caminhoLeitura;
    }
    public String getPastaEscrita() {
        return pastaEscrita;
    }
    public void setPastaEscrita(String pastaEscrita) {
        this.pastaEscrita = pastaEscrita;
    }
    public String getArquivoEntradaCompleto() {
        return arquivoEntradaCompleto;
    }
    public void setArquivoEntradaCompleto(String arquivoEntradaCompleto) {
        this.arquivoEntradaCompleto = arquivoEntradaCompleto;
    }
    public String getConfigurar() {
        return configurar;
    }
    public void setConfigurar(String configurar) {
        this.configurar = configurar;
    }
    public int getnArcos() {
        return nArcosTotal;
    }
    public void setnArcos(int nArcos) {
        this.nArcosTotal = nArcos;
    }
    public int getnVariaveis() {
        return nVariaveis;
    }
    public void setnVariaveis(int nVariaveis) {
        this.nVariaveis = nVariaveis;
    }
    public float getPasso() {
        return passo;
    }
    public void setPasso(float passo) {
        this.passo = passo;
    }
    public HashMap<String, String> getTransistoresSizing() {
        return transistoresSizing;
    }
    public void geracaoBatWindows(String pastaEscrita, String PrefixFile, int totArquivos, int nSimulacoes) 
    {
       
        for(int j=0; j<nSimulacoes ;j++)
        {
            String write = "";
            
            for(int i=0; i<(int)(totArquivos/nSimulacoes); i++)
            {
                 write += "tspcmd64.exe.lnk "+pastaEscrita+"\\Spices\\"+PrefixFile+"_"
                                +((int)(totArquivos/nSimulacoes)*j+i)+".sp\n";
            }
            if(j==nSimulacoes-1)
            {
                for(int i=totArquivos-(int)(totArquivos%nSimulacoes); i<totArquivos; i++)
                {
                     write += "tspcmd64.exe.lnk "+pastaEscrita+"\\Spices\\"+PrefixFile+"_"+i+".sp\n";
                }
                write += "java -jar \""+pastaEscrita+"\\simulacao.spice.jar\"";
            }
            
            io.escreverArquivo("\\"+pastaEscrita+"\\Spices\\simulador_"+j+".bat", write);
        }
    }
    public void geracaoBatLinux(String pastaEscrita, String PrefixFile, int totArquivos, int nSimulacoes) 
    {
       
        for(int j=0; j<nSimulacoes ;j++)
        {
            String write = "source /scripts/set_cadence.bash\n\n";
                       
            for(int i=0; i<(int)(totArquivos/nSimulacoes); i++)
            {
                 write += "spectre -64 "+pastaEscrita+"Spices/"+PrefixFile+"_"
                                +((int)(totArquivos/nSimulacoes)*j+i)+".sp\n";
            }
            if(j==nSimulacoes-1)
            {
                for(int i=totArquivos-(int)(totArquivos%nSimulacoes); i<totArquivos; i++)
                {
                     write += "spectre -64 "+pastaEscrita+"Spices/"+PrefixFile+"_"+i+".sp\n";
                }
            }
            
            io.escreverArquivo("/"+pastaEscrita+"Spices/simulador_"+j+".txt", write);
        }
    }
    public void setTransistoresSizing(HashMap<String, String> transistoresSizing) {
        this.transistoresSizing = transistoresSizing;
    }

    public List<Transistor> getListaTransistor() {
        return listaTransistor;
    }

    public void setListaTransistor(List<Transistor> listaTransistor) {
        this.listaTransistor = listaTransistor;
    }

    public String getPrefixName() {
        return prefixName;
    }

    public void setPrefixName(String prefixName) {
        this.prefixName = prefixName;
    }

    public int getTotArquivos() {
        return totArquivos;
    }

    public void setTotArquivos(int totArquivos) {
        this.totArquivos = totArquivos;
    }

    public int getnArcosTotal() {
        return nArcosTotal;
    }

    public void setnArcosTotal(int nArcosTotal) {
        this.nArcosTotal = nArcosTotal;
    }

    public int getnArcosPCenario() {
        return nArcosPCenario;
    }

    public void setnArcosPCenario(int nArcosPCenario) {
        this.nArcosPCenario = nArcosPCenario;
    }
    
}
